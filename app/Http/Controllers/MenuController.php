<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudActions;
use App\Models\Menu;
use App\Models\Ressource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class MenuController extends Controller
{
    use CrudActions;
    public $crudId = 'menu';
    public $viewsData = [];
    public function __construct() 
    {
        $this->middleware('auth');
        $this->viewsData['menus'] = \App\Models\Menu::all()->where('deleted','0');
    }

    public function list(Request $request){
        //$res = "SMenu donnés d'inventaire2";
        //dd(\Auth::user()::hasRessource($res), session('user_ressources'), session('user_routes'));
        $request->flash();
        return view('back/menu/list' , 
                                        [
                                            'records' => Menu::all()->where('deleted',"0")
                                        ]
        );
    }

    public function update($recordId, Request $request){
        $record = ($this->getClassName())::find($recordId);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [];
            $validator =  Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            else{
                $record->update($request->all());
                Redirect::to(route('menu_list'))->send();
            }            
        }
        $routeCollection = \Illuminate\Support\Facades\Route::getRoutes();
        $this->viewsData['record'] = $record;
        $this->viewsData['routes'] = $routeCollection;
        $this->viewsData['ressources'] = Ressource::all()->where('deleted',"0");
        return view('back/menu/update' , $this->viewsData);        
    }
    
    public function create(Request $request){
        
        $routeCollection = \Illuminate\Support\Facades\Route::getRoutes();
        if ($request->isMethod('post')) { 
            $rules = [];
            $validator =  Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            else{
                ($this->getClassName())::create($request->all());
                Redirect::to(route($this->crudId.'_list'))->send();
            }            
        }
        $this->viewsData['routes'] = $routeCollection;
        $this->viewsData['ressources'] = Ressource::all()->where('deleted',"0");
        return view('back/'.$this->crudId.'/create', $this->viewsData);        
    }

    

    protected function getRules()
    {
        return [
        ];
    }
}