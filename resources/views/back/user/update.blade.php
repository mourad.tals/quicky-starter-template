@extends($layout)

@section('content')

<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">

                <form method="POST" action="{{ route('user_update', ['user' => $user->id]) }}">
                    @csrf
                    <br>

                    @if (Session::has('success'))
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p>{{ Session::get('success') }} </p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif

                    <div class="card">
                        <div class="card-panel">

                            <div class="row">


                                <div class="col s12 m12">

                                    <div class="col s6 input-field">
                                        <input id="first_name" name="first_name" type="text"
                                            value="{{ old('first_name', $user->first_name) }}">

                                        <label for="first_name">Prénom</label>
                                        @error('first_name')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col s6 input-field">
                                        <input id="name" value="{{ old('name', $user->name) }}" name="name" type="text">
                                        <label for="name">Nom </label>
                                        @error('name')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col s12 m12">

                                    <div class="col s6 input-field">
                                        <input id="login" name="login" value="{{ old('login', $user->login) }}"
                                            autocomplete="off" readonly onfocus="this.removeAttribute('readonly');"
                                            type="text">
                                        <label for="login">Identifiant </label>
                                        @error('login')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col s6 input-field" id='div_role'>
                                        <select style="width: 100%;" name="role" id='role'
                                            class="select2 browser-default">
                                            @foreach ($roles as $role)
                                            <option class="option"
                                                {{ $role->id == old('role', $user->role) ? 'selected' : '' }}
                                                value="{{ $role->id }}">{{ $role->label }}</option>
                                            @endforeach
                                        </select>
                                        <label>Role</label>
                                        @error('role')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>



                                <div class="col s12 m12">

                                    <div class="col s6 input-field">
                                        <input id="password" name="password" readonly
                                            onfocus="this.removeAttribute('readonly');" autocomplete="off"
                                            type="password">
                                        <label for="password"> Mot de passe</label>
                                        @error('password')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col s6 input-field">
                                        <input id="password_confirmation" readonly
                                            onfocus="this.removeAttribute('readonly');" name="password_confirmation"
                                            type="password" class="validate">
                                        <label for="password_confirmation"> Confirmation mot de passe</label>
                                        @error('password_confirmation')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                

                            </div>


                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <a href="{{ route('user_list') }}"><button type="button"
                                                class="btn btn-light">Retour </button></a>
                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            Enregistrer</button>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>

@stop
@section('js')
<link rel="stylesheet" type="text/css" href="/assets/vendors/duallistbox/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/assets/vendors/duallistbox/bootstrap-duallistbox.css">
<script src="/assets/vendors/duallistbox/jquery.bootstrap-duallistbox.js"></script>

<style>
#villes .select-dropdown {
    display: none !important;
}

.info {
    display: none !important;
}
</style>
<script>
if ($('#type').val() === '1') {
    $('#client_row').css('display', 'none');
    $('#employe_row').css('display', 'block');
    $('#villes').show();
    $('#role').prop('disabled', false);
} else {
    $('#client_row').css('display', 'block');
    $('#employe_row').css('display', 'none');
    $('#villes').hide()
    $("#role").select2("val", "3");
    $('#role').prop('disabled', 'disabled');
}
$(document).ready(function() {
    $('select[name="villes[]"]').bootstrapDualListbox();
    $('select[name="villes_charger[]"]').bootstrapDualListbox();

    $("#type").change(function(e) {
        if ($(this).val() === '1') {
            $('#client_row').css('display', 'none');
            $('#employe_row').css('display', 'block');
            $('#autorisations').show();
            $('#role').prop('disabled', false);
            $("#role").select2("val", "1");
        } else {
            $('#client_row').css('display', 'block');
            $('#employe_row').css('display', 'none');
            $('#autorisations').hide();
            $("#role").select2("val", "3");
            $('#role').prop('disabled', 'disabled');

        }
    });


});
</script>
@stop
