@extends($layout)
@section('content')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <!--card stats start-->
                <div id="card-stats" class="pt-0">
                    <div class="row">
                        <div class="col s12 m6 l6 xl2">
                            <div class="card animate fadeLeft">
                                <div class="card-content cyan white-text">
                                    <p class="card-stats-title">
                                        <i class="material-icons">local_shipping</i> Colis
                                    </p>
                                    <h4 class="card-stats-number white-text">1590</h4>
                                    <p class="card-stats-compare">
                                        <span class="cyan text text-lighten-5">Total en cours</span>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m6 l6 xl2">
                            <div class="card animate fadeLeft">
                                <div class="card-content red accent-2 white-text">
                                    <p class="card-stats-title">
                                        <i class="material-icons">local_shipping</i>Colis
                                    </p>
                                    <h4 class="card-stats-number white-text">1573</h4>
                                    <p class="card-stats-compare">
                                        <span class="red-text text-lighten-5">ECOM en cours</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl2">
                            <div class="card animate fadeRight">
                                <div class="card-content orange lighten-1 white-text">
                                    <p class="card-stats-title">
                                        <i class="material-icons">local_post_office</i> Colis
                                    </p>
                                    <h4 class="card-stats-number white-text">17</h4>
                                    <p class="card-stats-compare">
                                        <span class="orange-text text-lighten-5">ADM en cours</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl2">
                            <div class="card animate fadeRight">
                                <div class="card-content green lighten-1 white-text">
                                    <p class="card-stats-title">
                                        <i class="material-icons">attach_money</i>
                                    </p>
                                    <h4 class="card-stats-number white-text" style="font-size: 1.6rem;"> 1 553 841Dh
                                    </h4>
                                    <p class="card-stats-compare">
                                        <span class="green-text text-lighten-5">Remb. en attente</span>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m6 l6 xl2">
                            <div class="card animate fadeRight">
                                <div class="card-content blue lighten-1 white-text">
                                    <p class="card-stats-title">
                                        <i class="material-icons">attach_money</i>
                                    </p>
                                    <h4 class="card-stats-number white-text">1 180,00Dh</h4>
                                    <p class="card-stats-compare">
                                        <span class="green-text text-lighten-5">Total à facturer</span>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m6 l6 xl2">
                            <div class="card animate fadeRight">
                                <div class="card-content red lighten-1 white-text">
                                    <p class="card-stats-title">
                                        <i class="material-icons">input</i>Colis
                                    </p>
                                    <h4 class="card-stats-number white-text">236</h4>
                                    <p class="card-stats-compare">
                                        <span class="green-text text-lighten-5">Total à ramasser</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--card stats end-->
            <!--yearly & weekly revenue chart start-->
            <div id="sales-chart">
                <div class="row">
                    <div class="col s12 m8 l6">
                        <h4 class="header mt-0">
                            Réalisations mensuel </h4>

                        <canvas id="Rmensuel"></canvas>

                    </div>
                    <div class="col s12 m4 l6">
                        <div class="card-content">
                            <h4 class="header m-0">
                                Evolution Chiffre d'affaires
                            </h4>
                            <canvas id="EvolutionChiffre"></canvas>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m8 l6">

                        <div id="bordered-table" class="card card card-default scrollspy">
                            <div class="card-content">
                                <h4 class="card-title"> Alertes</h4>

                                <div class="row">

                                    <div class="col s12">
                                        <table class="bordered">
                                            <thead>
                                                <tr>
                                                    <th data-field="id">Titre</th>
                                                    <th data-field="name">Nombre</th>
                                                    <th data-field="price">Rapport</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td> Souffrances ramassage (+24H)</td>
                                                    <td style="text-align: center;color: red; ">3</td>
                                                    <td style="text-align: center;"><span
                                                            class="material-icons">local_printshop</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Non livrés</td>
                                                    <td style="text-align: center;color: red; ">3</td>
                                                    <td style="text-align: center;"><span
                                                            class="material-icons">local_printshop</span></td>
                                                </tr>
                                                <tr>
                                                    <td> Caisses non validées (+24H)</td>
                                                    <td style="text-align: center;color: red; ">3</td>
                                                    <td style="text-align: center;"><span
                                                            class="material-icons">local_printshop</span></td>
                                                </tr>
                                                <tr>
                                                    <td> PP Non livrés</td>
                                                    <td style="text-align: center;color: red; ">3</td>
                                                    <td style="text-align: center;"><span
                                                            class="material-icons">local_printshop</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col s12 m8 l6" style="margin-top: 30px;margin-bottom: 30px;">
                        <h4 class="header m-0">
                            Taux de retour
                        </h4>
                        <canvas id="Tauxretour"></canvas>
                    </div>
                </div>

            </div>

            <!-- Member online, Currunt Server load & Today's Revenue Chart start -->
            <!-- ecommerce product start-->

            <!--end container-->
        </div>
    </div>
    <div class="content-overlay"></div>
</div>
@stop

@section('js')
<script src="/assets/js/scripts/dashboard-ecommerce.js"></script>
<script>
//realisation mensuels chart
const labels = [
    '3/21',
    '4/21',
    '5/21',
    '6/21',
    '7/21',
    '8/21',
    '9/21',
    '10/21',
    '11/21',
    '12/21',
    '1/22',
    '2/22',
];

const data = {
    labels: labels,
    datasets: [{
        label: 'E-commerce',
        backgroundColor: '#66bb6a',
        borderColor: 'rgb(255, 99, 132)',
        data: [6000, 4999, 5456, 7882, 10230, 3660, 4555, 11292, 4399, 9847, 5004, 4090],
    }, {
        label: 'Docs. Administratifs',
        backgroundColor: '#ffa726',
        borderColor: 'rgb(255, 99, 132)',
        data: [1000, 3999, 7456, 5882, 17230, 9860, 6555, 11292, 4799, 1847, 2004, 9090],
    }, {
        label: 'Retour',
        backgroundColor: '#ff5252',
        borderColor: 'rgb(255, 99, 132)',
        data: [4000, 5999, 7656, 2382, 9230, 5460, 3555, 3292, 11399, 5847, 7004, 2090],
    }]
};

const config = {
    type: 'bar',
    data: data,
    options: {}
};


//Evolution Chiffrechart
const labelsEvolutionChiffre = [
    '3/21',
    '4/21',
    '5/21',
    '6/21',
    '7/21',
    '8/21',
    '9/21',
    '10/21',
    '11/21',
    '12/21',
    '1/22',
    '2/22',
];

const dataEvolutionChiffre = {
    labels: labelsEvolutionChiffre,
    datasets: [{
        label: 'C.A en Dh',
        //   backgroundColor: '#ff5252 ',
        borderColor: '#ff5252',
        data: [320004, 330004, 320004, 370004, 330004, 350004, 370004, 250004, 550004, 550004, 650004,
            750004
        ],
    }, {
        label: 'CA Encaissé Dh',
        //   backgroundColor: '#42a5f5 ',
        borderColor: '#00bcd4',
        data: [320004, 350004, 370004, 340004, 370004, 360004, 310004, 360004, 330004, 390004, 340004,
            320004
        ],
    }]
};

const configEvolutionChiffre = {
    type: 'line',
    data: dataEvolutionChiffre,
    options: {

    }
};

//Taux de retour
const labelsTauxretour = [
    'Taux de retour',
];

const dataTauxretour = {
    labels: labelsTauxretour,
    datasets: [{

        backgroundColor: ['rgba(255, 99, 132, 0.5)', 'rgba(54, 162, 235, 0.2)'],
        borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)'],
        data: [19.5, 80],
    }]
};

const configTauxretour = {
    type: 'pie',
    data: dataTauxretour,
    options: {

    }
};
</script>
<script>
const Rmensuel = new Chart(
    document.getElementById('Rmensuel'),
    config
);

const EvolutionChiffre = new Chart(
    document.getElementById('EvolutionChiffre'),
    configEvolutionChiffre
);
const Tauxretour = new Chart(
    document.getElementById('Tauxretour'),
    configTauxretour
);
</script>
@stop