@extends($layout)
@section('content')

<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <br>
                <div class="list-table" id="app">
                    <div class="card">
                        <div class="card-content">
                            <!-- datatable start -->
                            <div class="responsive-table">
                                <table id="list-datatable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            
                                            <th> Libellé </th>
                                            <!--<th> Statut </th> -->
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($records as $record)
                                        <tr>
                                            
                                            <td> {{ $record->label }} </td>
                                            <!--<td> {{ $record->statut }} </td>-->
                                            <td>
                                                <a href="{{route('apparence_update', ['apparence'=>$record->id])}}"><i
                                                        class="material-icons tooltipped" data-position="top"
                                                        data-tooltip="تعديل">edit</i></a>
                                                <a href="#!" onclick="openSuppModal({{$record->id}})"><i
                                                        class="material-icons tooltipped" style="color: #c10027;"
                                                        data-position="top" data-tooltip="حدف">delete</i></a>
                                            </td>
                                        </tr>
                                        @endforeach

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a
                href="{{route('apparence_create')}}"
                class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i
                    class="material-icons">add</i></a></div>
    </div>
</div>

<div id="delete_modal" class="modal">
    <div class="modal-content">
        <h4> تأكيد الحذف</h4>
        <div>
            هل أنت متأكد من الحذف ؟ </div>
        <input type="hidden" name="delId" id="delId">
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn grey">رجوع </a>
        <a href="#!" class="waves-effect waves-green btn red" onclick="suppRecord()">حدف</a>
    </div>
</div>
@stop
@section('js')
<script>
function openSuppModal(id) {
    $("#delId").val(id);
    $('#delete_modal').modal('open');
}

function suppRecord() {
    window.location.replace("/apparence/delete/" + $("#delId").val());
}
$(document).ready(function() {
    $('.modal').modal();
});
</script>
@stop