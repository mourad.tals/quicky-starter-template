<?php

namespace EasyCollab\Quicky;

use App\Http\Controllers\Controller;
use App\Models\Quickyproject;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use EasyCollab\Quicky\Models\Quicky;

class QuickyController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            if (isset($_POST['Identifiant'])) {
                $project = Quickyproject::all()->where('deleted', "0")->where('name', $_POST['projetid'])->first();
                if ($project) {
                    return response("
                            <script>
                                alert('The project already exists.');
                                window.location.href='/quicky';
                            </script>
                        ");
                } else {
                    //\DB::statement(Quicky::getSql());
                    Quicky::genMigrationFile();
                    Quicky::addRoutes();
                    Quicky::genModelFile();
                    Quicky::genControllerFile();
                    Quicky::genListView();
                    Quicky::genActions();
                    Quicky::genCreateView();
                    Quicky::genUpdateView();
                    Quicky::addProjectToList($_POST['projetid']);
                    Artisan::call('migrate --force');
                    Artisan::call('route:clear');
                }
            }
        }
        return view('easycollab::create');
    }

    public function checkProject($projectName)
    {
        $project = Quickyproject::where('name', $projectName)->first();
        return response()->json(['exists' => !is_null($project)]);
    }


}
