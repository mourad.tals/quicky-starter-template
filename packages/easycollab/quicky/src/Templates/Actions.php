<div>
    <a href="{{ route('{projetId}.edit', ['{projetId}' => ${projetId}->id]) }}">
        <i class="material-icons tooltipped" data-position="top" data-tooltip="Modifier">edit</i>
    </a>
    <a href="#!" onclick="openSuppModal({{ ${projetId}->id }})">
        <i class="material-icons tooltipped" style="color: #c10027;" data-position="top" data-tooltip="Supprimer">delete</i>
    </a>
</div>
